package repository

import (
	"main/models"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// type Authorization interface {
// 	CreateUser(user models.User) (int, error)
// 	GetUser(username, password string) (models.User, error)
// }

type Books interface {
	GetTopAuthors() ([]models.AuthorRentCount, error)
	InitializeAuthors() error
	InitializeUsers() error
	GetUsers() ([]models.User, error)
	AddAuthor(name, description string) (models.Author, error)
	GetAuthors() ([]struct {
		Author models.Author
		Books  []struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		} `json:"books"`
	}, error)
	AddBooksIfEmpty() error
	AddBookWithAuthorID(*models.Book, int) (*models.Book, error)
	GetBooks() ([]models.Book, error)
	RentBook(bookID, userID int) error
	ReturnBook(bookId, userId int) error
	// InitializeBooks() error

	// GetBooksByAuthorID(int) ([]models.Book, error)
	// GetAuthorByID(authorID int) (models.Author, error)
	// GetBooks() ([]models.Book, error)
	// GetRentedBookIDsByUserID(userID int) ([]int, error)
	// GetBooksByID(bookIDs []int) ([]models.Book, error)

}

type Repository struct {
	// Authorization
	Books
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		// Authorization: NewAuthPostgres(db),
		Books: NewBooksPostgres(db),
	}
}
