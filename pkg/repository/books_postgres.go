package repository

import (
	"errors"
	"fmt"
	"main/models"
	"math/rand"
	"sort"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/jmoiron/sqlx"
)

type BooksPostgres struct {
	db *sqlx.DB
}

func NewBooksPostgres(db *sqlx.DB) *BooksPostgres {
	return &BooksPostgres{db: db}
}

func (db *BooksPostgres) GetBooks() ([]models.Book, error) {
	rows, err := db.db.Query(`
		SELECT books.id, books.title, books.author_id, books.rentalscount, authors.name, authors.description
		FROM books INNER JOIN authors ON books.author_id = authors.id
	`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	books := make([]models.Book, 0)
	for rows.Next() {
		var book models.Book
		var authorName string
		err := rows.Scan(&book.ID, &book.Title, &book.AuthorID, &book.RentalsCount, &authorName, &book.Author.Description)
		if err != nil {
			return nil, err
		}
		book.Author = models.Author{Name: authorName, ID: book.AuthorID, Description: book.Author.Description}
		books = append(books, book)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return books, nil
}

// InitializeAuthors добавляет 10 авторов в таблицу авторов, если она пуста.
func (db *BooksPostgres) InitializeAuthors() error {
	var count int
	err := db.db.QueryRow("SELECT COUNT(*) FROM authors").Scan(&count)
	if err != nil {
		return err
	}

	if count == 0 {
		for i := 0; i < 11; i++ {
			author := models.Author{
				Name:        gofakeit.Name(),
				Description: gofakeit.Sentence(10),
			}
			fmt.Printf("Generated description: %v\n", author.Description)
			_, err := db.db.Exec("INSERT INTO authors (name, description) VALUES ($1, $2)", author.Name, author.Description)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (bp *BooksPostgres) AddAuthor(name, description string) (models.Author, error) {
	if name == "" {
		return models.Author{}, errors.New("name cannot be empty")
	}

	query := "INSERT INTO authors (name, description) VALUES ($1, $2) RETURNING id"
	row := bp.db.QueryRowx(query, name, description)
	author := models.Author{
		Name:        name,
		Description: description,
	}
	err := row.Scan(&author.ID)
	if err != nil {
		return models.Author{}, err
	}

	return author, nil
}

func (db *BooksPostgres) GetAuthorByID(authorID int) (models.Author, error) {
	var author models.Author
	err := db.db.QueryRow("SELECT id, name FROM authors WHERE id = $1", authorID).Scan(&author.ID, &author.Name)
	if err != nil {
		return models.Author{}, err
	}

	return author, nil
}

func (r *BooksPostgres) GetBooksByID(bookIDs []int) ([]models.Book, error) {
	var books []models.Book
	query, args, err := sqlx.In("SELECT * FROM books WHERE id IN (?)", bookIDs)
	if err != nil {
		return nil, err
	}
	query = r.db.Rebind(query)
	err = r.db.Select(&books, query, args...)
	if err != nil {
		return nil, err
	}
	return books, nil
}

func (r *BooksPostgres) GetRentedBookIDsByUserID(userID int) ([]int, error) {
	var bookIDs []int
	err := r.db.Select(&bookIDs, "SELECT book_id FROM rentals WHERE user_id=$1 AND return_date IS NULL", userID)
	if err != nil {
		return nil, err
	}

	return bookIDs, nil
}

func (bp *BooksPostgres) AddBookAuthor(bookID int, authorID int) error {
	query := "INSERT INTO book_authors (book_id, author_id) VALUES ($1, $2)"
	_, err := bp.db.Exec(query, bookID, authorID)
	if err != nil {
		return err
	}

	return nil
}

// GetAuthors возвращает список авторов с информацией о книгах каждого автора.
func (db *BooksPostgres) GetAuthors() ([]struct {
	Author models.Author
	Books  []struct {
		ID    int    `json:"id"`
		Title string `json:"title"`
	} `json:"books"`
}, error) {
	rows, err := db.db.Query("SELECT id, name, description FROM authors")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var ExpandAuthors []struct {
		Author models.Author
		Books  []struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		} `json:"books"`
	}
	for rows.Next() {
		var author models.Author

		err := rows.Scan(&author.ID, &author.Name, &author.Description)
		if err != nil {
			return nil, err
		}

		books, err := db.GetBooksByAuthorID(author.ID)
		if err != nil {
			return nil, err
		}

		var bookList []struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		}

		for _, book := range books {
			bookList = append(bookList, struct {
				ID    int    `json:"id"`
				Title string `json:"title"`
			}{
				ID:    book.ID,
				Title: book.Title,
			})
		}

		tempBook := struct {
			Author models.Author
			Books  []struct {
				ID    int    `json:"id"`
				Title string `json:"title"`
			} `json:"books"`
		}{
			Author: author,
			Books:  bookList,
		}

		ExpandAuthors = append(ExpandAuthors, tempBook)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return ExpandAuthors, nil
}

func (db *BooksPostgres) GetBooksByAuthorID(authorID int) ([]models.Book, error) {
	rows, err := db.db.Query("SELECT id, title FROM books WHERE author_id = $1", authorID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []models.Book
	for rows.Next() {
		var book models.Book
		err := rows.Scan(&book.ID, &book.Title)
		if err != nil {
			return nil, err
		}

		book.AuthorID = authorID
		books = append(books, book)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return books, nil
}

func (db *BooksPostgres) AddBooksIfEmpty() error {
	var count int
	err := db.db.QueryRow("SELECT COUNT(*) FROM books").Scan(&count)
	if err != nil {
		return err
	}

	if count == 0 {
		authors, err := db.GetAuthors()
		if err != nil {
			return err
		}

		for i := 0; i < 101; i++ {
			author := authors[rand.Intn(len(authors))]
			book := models.Book{
				Title:    gofakeit.Sentence(5),
				AuthorID: author.Author.ID,
				Author:   author.Author,
			}
			err = db.AddBook(&book)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (db *BooksPostgres) AddBook(book *models.Book) error {
	err := db.db.QueryRow(
		"INSERT INTO books(title, author_id) VALUES ($1, $2) RETURNING id",
		book.Title, book.AuthorID).Scan(&book.ID)
	if err != nil {
		return err
	}
	return nil
}

func (db *BooksPostgres) AddBookWithAuthorID(book *models.Book, authorID int) (*models.Book, error) {
	author, err := db.GetAuthorByID(authorID)
	if err != nil {
		return nil, err
	}
	fmt.Println(book)

	book.AuthorID = author.ID
	book.Author = author

	err = db.AddBook(book)
	if err != nil {
		return nil, err
	}

	return book, nil

}

func (db *BooksPostgres) AddUser(name string) (models.User, error) {
	if name == "" {
		return models.User{}, errors.New("name cannot be empty")
	}

	query := "INSERT INTO users (name) VALUES ($1) RETURNING id"
	row := db.db.QueryRowx(query, name)
	user := models.User{
		Name: name,
	}
	err := row.Scan(&user.ID)
	if err != nil {
		return models.User{}, err
	}

	return user, nil
}

func (db *BooksPostgres) GetUsers() ([]models.User, error) {
	rows, err := db.db.Query("SELECT id, name FROM users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var user models.User
		err := rows.Scan(&user.ID, &user.Name)
		if err != nil {
			return nil, err
		}

		rentedBookIDs, err := db.GetRentedBookIDsByUserID(user.ID)
		if err != nil {
			return nil, err
		}

		if len(rentedBookIDs) > 0 {
			books, err := db.GetBooksByID(rentedBookIDs)
			if err != nil {
				return nil, err
			}

			user.RentedBooks = books
		}

		users = append(users, user)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return users, nil
}

func (db *BooksPostgres) InitializeUsers() error {
	var count int
	err := db.db.QueryRow("SELECT COUNT(*) FROM users").Scan(&count)
	if err != nil {
		return err
	}

	if count == 0 {
		for i := 0; i < 51; i++ {
			user := models.User{
				Name: gofakeit.Name(),
			}
			_, err := db.db.Exec("INSERT INTO users (name) VALUES ($1)", user.Name)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// ---------------------------------//////////////////////////////////
func (db *BooksPostgres) RentBook(bookID, userID int) error {
	// Check if the book is available
	var count int
	err := db.db.QueryRow("SELECT COUNT(*) FROM rentals WHERE book_id=$1 AND return_date IS NULL", bookID).Scan(&count)
	if err != nil {
		return err
	}
	if count > 0 {
		return errors.New("book is already rented")
	}

	// Check if the user has already rented a book
	err = db.db.QueryRow("SELECT COUNT(*) FROM rentals WHERE user_id=$1 AND return_date IS NULL", userID).Scan(&count)
	if err != nil {
		return err
	}
	if count > 0 {
		return errors.New("user has already rented a book")
	}

	// Rent the book and increment the RentalsCount
	_, err = db.db.Exec("UPDATE books SET rentalscount = rentalscount + 1 WHERE id = $1", bookID)
	if err != nil {
		return err
	}
	_, err = db.db.Exec("INSERT INTO rentals (user_id, book_id, rental_date) VALUES ($1, $2, $3)", userID, bookID, time.Now())
	if err != nil {
		return err
	}

	return nil
}

func (db *BooksPostgres) ReturnBook(bookID, userID int) error {
	// Check if the user has rented the book
	var count int
	err := db.db.QueryRow("SELECT COUNT(*) FROM rentals WHERE user_id=$1 AND book_id=$2 AND return_date IS NULL", userID, bookID).Scan(&count)
	if err != nil {
		return err
	}

	if count == 0 {
		return errors.New("user has not rented this book")
	}
	// Update the rental record with the return date
	_, err = db.db.Exec("UPDATE rentals SET return_date=$1 WHERE user_id=$2 AND book_id=$3 AND return_date IS NULL", time.Now(), userID, bookID)
	if err != nil {
		return err
	}
	return nil
}

func (db *BooksPostgres) GetUserRentedBooks(userID int) ([]models.Book, error) {
	// Check if the user exists
	var count int
	err := db.db.QueryRow("SELECT COUNT(*) FROM users WHERE id=$1", userID).Scan(&count)
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, errors.New("user not found")
	}

	// Get the rented books for the user
	rows, err := db.db.Query("SELECT b.id, b.title, b.author, b.published_year FROM rentals r JOIN books b ON r.book_id=b.id WHERE r.user_id=$1 AND r.return_date IS NULL", userID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	books := make([]models.Book, 0)
	for rows.Next() {
		var book models.Book
		err := rows.Scan(&book.ID, &book.Title, &book.Author) //&book.PublishedYear
		if err != nil {
			return nil, err
		}
		books = append(books, book)
	}

	return books, nil
}

func (db *BooksPostgres) GetTopAuthors() ([]models.AuthorRentCount, error) {
	books, err := db.GetBooks()
	if err != nil {
		return nil, err
	}

	authors := make(map[int]int)
	for _, book := range books {
		if _, ok := authors[book.Author.ID]; !ok {
			authors[book.Author.ID] = book.RentalsCount + 1
		} else {
			authors[book.Author.ID] += book.RentalsCount
		}
	}
	fmt.Println()
	// Создаем срез авторов из данных о книгах
	topAuthors := make([]models.AuthorRentCount, 0, len(authors))
	for _, book := range books {
		author := book.Author
		if _, ok := authors[author.ID]; ok {
			authorRentCount := models.AuthorRentCount{
				Name:         author.Name,
				RentalsCount: authors[author.ID],
			}
			topAuthors = append(topAuthors, authorRentCount)
			delete(authors, author.ID)
		}
	}

	// Сортируем авторов в порядке убывания количества аренд
	sort.Slice(topAuthors, func(i, j int) bool {
		return topAuthors[i].RentalsCount > topAuthors[j].RentalsCount
	})

	return topAuthors, nil
}

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// -----------------------///

// func (db *BooksPostgres) GetUsers() ([]models.User, error) {
// 	rows, err := db.db.Query("SELECT id, name FROM users")
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer rows.Close()

// 	var users []models.User
// 	for rows.Next() {
// 		var user models.User
// 		err := rows.Scan(&user.ID, &user.Name)
// 		if err != nil {
// 			return nil, err
// 		}

// 		rentedBookIDs, err := db.GetRentedBookIDsByUserID(user.ID)
// 		if err != nil {
// 			return nil, err
// 		}

// 		if len(rentedBookIDs) > 0 {
// 			books, err := db.GetBooksByID(rentedBookIDs)
// 			if err != nil {
// 				return nil, err
// 			}

// for i, book := range books {
// 	author, err := db.GetAuthorByID(book.AuthorID)
// 	if err != nil {
// 		return nil, err
// 	}
// 	// books[i].Author = author
// }

// 			user.RentedBooks = books
// 		}

// 		users = append(users, user)
// 	}
// 	if err := rows.Err(); err != nil {
// 		return nil, err
// 	}
// 	return users, nil
// }

// // GetBooksByAuthorID возвращает список книг, написанных автором с указанным идентификатором.
// func (db *BooksPostgres) GetBooksByAuthorID(id int) ([]models.Book, error) {
// 	return nil, nil
// }

// 	rows, err := db.db.Query("SELECT id, title FROM books WHERE author_id = $1", authorID)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer rows.Close()

// 	var books []models.Book
// 	for rows.Next() {
// 		var book models.Book
// 		err := rows.Scan(&book.ID, &book.Title)
// 		if err != nil {
// 			return nil, err
// 		}

// 		author, err := db.GetAuthorByID(authorID)
// 		if err != nil {
// 			return nil, err
// 		}
// 		book.Author = &author

// 		books = append(books, book)
// 	}
// 	if err := rows.Err(); err != nil {
// 		return nil, err
// 	}

// 	return nil, nil
// }

// GetAuthorByID возвращает автора с указанным идентификатором.

// InitializeBooks добавляет 100 книг в таблицу книг, если она пуста, и связывает их с существующими авторами.
// func (db *BooksPostgres) InitializeBooks() error {
// 	var count int
// 	err := db.db.QueryRow("SELECT COUNT(*) FROM books").Scan(&count)
// 	if err != nil {
// 		return err
// 	}

// 	if count == 0 {
// 		authors, err := db.GetAuthors()
// 		if err != nil {
// 			return err
// 		}

// 		for i := 0; i < 100; i++ {
// 			author := authors[gofakeit.Number(0, len(authors)-1)]
// 			book := gofakeit.Sentence(5) // Генерация случайного предложения длиной 5 слов
// 			_, err := db.db.Exec("INSERT INTO books (title, author_id) VALUES ($1, $2)", book, author.ID)
// 			if err != nil {
// 				return err
// 			}
// 		}
// 	}

// 	return nil
// }

// GetBooks возвращает список всех книг с информацией об авторе каждой книги.
// func (db *BooksPostgres) GetBooks() ([]models.Book, error) {
// 	rows, err := db.db.Query("SELECT books.id, books.title, authors.id, authors.name FROM books JOIN authors ON books.author_id = authors.id")
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer rows.Close()

// 	var books []models.Book
// for rows.Next() {
// 	var book models.Book
// 	var author models.Author
// 	err := rows.Scan(&book.ID, &book.Title, &author.ID, &author.Name)
// 	if err != nil {
// 		return nil, err
// 	}

// 	author.Books, err = db.GetBooksByAuthorID(author.ID)
// 	if err != nil {
// 		return nil, err
// 	}
// 	book.Author = &author

// 	books = append(books, book)
// }
// if err := rows.Err(); err != nil {
// 	return nil, err
// }

// 	return books, nil
// }

///---------------------////
