package handler

import (
	_ "main/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func InitRoutes(r *gin.Engine, libraryHandler *LibraryHandler) {

	r.GET("/users", libraryHandler.GetUsers)

	r.GET("/top", libraryHandler.GetTopAuthors)

	r.POST("/author", libraryHandler.AddAuthor)

	r.POST("/addbook/:id", libraryHandler.AddBook)

	r.GET("/authors", libraryHandler.GetAuthors)

	r.GET("/books", libraryHandler.GetBooks)

	r.POST("/rentBook", libraryHandler.RentBook)

	r.POST("/returnBook", libraryHandler.ReturnBook)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
