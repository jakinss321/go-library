package handler

import (
	"fmt"
	"main/models"
	"main/pkg/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type LibraryHandler struct {
	service *service.LibraryService
}

func NewLibraryHandler(service *service.LibraryService) *LibraryHandler {
	return &LibraryHandler{
		service: service,
	}
}

// GetBooks возвращает список всех книг с информацией об авторе каждой книги.
// @Summary GetBooks
// @Description  функционал вывода списка книг
// @Accept       json
// @Produce      json
// @Success      200  {object} []models.Book
// @Router       /books [get]
func (h *LibraryHandler) GetBooks(c *gin.Context) {
	books, err := h.service.GetBooks()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, books)
}

// @Summary GetTopAuthors
// @Description  функционал вывода списка топа читаемых авторов
// @Accept       json
// @Produce      json
// @Success      200  {object}  []models.AuthorRentCount
// @Router       /top [get]
func (h *LibraryHandler) GetTopAuthors(c *gin.Context) {
	authors, err := h.service.GetTopAuthors()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, authors)
}

// GetAuthors возвращает список авторов с информацией о книгах каждого автора.
// @Summary GetAuthors
// @Description  функционал вывода списка авторов
// @Accept       json
// @Produce      json
// @Success      200  {object}  []models.Author
// @Router       /authors [get]
func (h *LibraryHandler) GetAuthors(c *gin.Context) {
	authors, err := h.service.GetAuthors()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, authors)
}

// // GetUsers возвращает список пользователей с информацией о книгах, которые они взяли в аренду.
// @Summary GetUsers
// @Description  функционал вывода списка пользователей с полем RentedBooks, которое является массивом книг []Book
// @Accept       json
// @Produce      json
// @Success      200  {object}  []models.User
// @Router       /users [get]
func (h *LibraryHandler) GetUsers(c *gin.Context) {
	users, err := h.service.GetUsers()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, users)
}

// @Summary Добавить автора
// @Description Функционал добавления автора
// @Accept json
// @Produce json
// @Param title body models.Author true "Add Author"
// @Success 200 {object} models.Author
// @Router /author [post]
func (h *LibraryHandler) AddAuthor(c *gin.Context) {
	var input struct {
		Name        string `json:"name" binding:"required"`
		Description string `json:"description" binding:"required"`
	}
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newAuthor, err := h.service.AddAuthor(input.Name, input.Description)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, newAuthor)
}

// @Summary AddBook
// @Description функционал добавления книг с указанием ID автора
// @Accept json
// @Produce json
// @Param id path int true "ID автора"
// @Param title body models.Book true "Add book"
// @Success 200 {object} models.Book
// @Failure 400 {string} string "Bad Request"
// @Router /addbook/{id} [post]
func (h *LibraryHandler) AddBook(c *gin.Context) {
	book := &models.Book{}
	err := c.BindJSON(book)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}
	authorID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid author ID"})
		return
	}
	book, err = h.service.AddBook(book, authorID)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Author not found"})
	}
	c.JSON(http.StatusOK, book)
}

type RentRequest struct {
	UserId string `json:"userid" binding:"required"`
	BookId string `json:"bookid" binding:"required"`
}

type response struct {
	MessageRent string
}

var res = response{
	MessageRent: "Book rented successfully",
}

// @Summary RentBook
// @Description функционал получения книги существующим пользователем библиотеки
// @Accept json
// @Produce json
// @Param title body RentRequest true "RentRequest"
// @Success 200 {object} response
// @Router /rentBook [post]
func (h *LibraryHandler) RentBook(c *gin.Context) {

	var req RentRequest

	if err := c.BindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}
	bookId, err := strconv.Atoi(req.BookId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid struct req"})
		return
	}
	userId, err := strconv.Atoi(req.UserId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid struct req"})
		return
	}

	if err := h.service.RentBook(bookId, userId); err != nil {
		fmt.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to rent the book"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": res})
}

type resReturn struct {
	MessageReturn string
}

var resr = resReturn{
	MessageReturn: "Book return successfully",
}

// @Summary ReturnBook
// @Description функционал возврата книги существующим пользователем библиотеки
// @Accept json
// @Produce json
// @Param title body RentRequest true "RentRequest"
// @Success 200 {object} resReturn
// @Router /returnBook [post]
func (h *LibraryHandler) ReturnBook(c *gin.Context) {
	type ReturnRequest struct {
		UserId string `json:"userid" binding:"required"`
		BookId string `json:"bookid" binding:"required"`
	}

	var req ReturnRequest

	if err := c.BindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}
	bookId, err := strconv.Atoi(req.BookId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid struct req"})
		return
	}
	userId, err := strconv.Atoi(req.UserId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid struct req"})
		return
	}

	if err := h.service.ReturnBook(bookId, userId); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to rent the book"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": resr})
}
