package service

import (
	"main/models"
	"main/pkg/repository"
)

type LibraryService struct {
	repo repository.Books
}

func NewLibraryService(repo repository.Books) *LibraryService {
	return &LibraryService{
		repo: repo,
	}
}

// GetAuthors возвращает список авторов с информацией о книгах каждого автора.
func (s *LibraryService) GetAuthors() ([]struct {
	Author models.Author
	Books  []struct {
		ID    int    `json:"id"`
		Title string `json:"title"`
	} `json:"books"`
}, error) {
	return s.repo.GetAuthors()
}

// GetUsers возвращает список пользователей с информацией о книгах, которые они взяли в аренду.
func (s *LibraryService) GetUsers() ([]models.User, error) {
	return s.repo.GetUsers()
}

func (s *LibraryService) GetTopAuthors() ([]models.AuthorRentCount, error) {
	return s.repo.GetTopAuthors()
}

func (s *LibraryService) ReturnBook(bookId, userId int) error {
	return s.repo.ReturnBook(bookId, userId)
}
func (s *LibraryService) RentBook(bookId, userId int) error {
	return s.repo.RentBook(bookId, userId)
}
func (s *LibraryService) GetBooks() ([]models.Book, error) {
	return s.repo.GetBooks()
}

func (s *LibraryService) AddAuthor(name, description string) (models.Author, error) {
	return s.repo.AddAuthor(name, description)
}

func (s *LibraryService) AddBook(book *models.Book, authorID int) (*models.Book, error) {
	return s.repo.AddBookWithAuthorID(book, authorID)
}

// func (s *LibraryService) AddBook(title string, authorID int) (models.Book, error) {
// 	if title == "" {
// 		return models.Book{}, errors.New("title cannot be empty")
// 	}

// 	// Проверим, существует ли автор с заданным ID
// 	author, err := s.repo.GetAuthorByID(authorID)
// 	if err != nil {
// 		return models.Book{}, err
// 	}

// 	// Создаем новую книгу в базе данных
// 	book := models.Book{
// 		Title:    title,
// 		AuthorID: authorID,
// 		Author:   author,
// 	}
// 	err = s.repo.AddBook(&book)
// 	if err != nil {
// 		return models.Book{}, err
// 	}

// 	// Добавляем новую книгу в список книг у автора
// 	author.Books = append(author.Books, book)

// 	// Сохраняем связь между книгой и автором
// 	err = s.repo.AddBookAuthor(book.ID, authorID)
// 	if err != nil {
// 		return models.Book{}, err
// 	}

// 	return book, nil
// }

// // GetAuthorByID возвращает автора с указанным идентификатором.
// func (s *LibraryService) GetAuthorByID(authorID int) (models.Author, error) {
// 	return s.repo.GetAuthorByID(authorID)
// }

// // GetBooksByAuthorID возвращает список книг, написанных автором с указанным идентификатором.
// func (s *LibraryService) GetBooksByAuthorID(authorID int) ([]models.Book, error) {
// 	return s.repo.GetBooksByAuthorID(authorID)
// }

// func (s *LibraryService) GetBooks() ([]models.Book, error) {
// 	return s.repo.GetBooks()
// }

// // InitializeBooks добавляет 100 книг в таблицу книг, если она пуста, и связывает их с существующими авторами.
// func (s *LibraryService) InitializeBooks() error {
// 	return s.repo.InitializeBooks()
// }
