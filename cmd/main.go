package main

import (
	"log"
	"main/pkg/handler"
	"main/pkg/repository"
	"main/pkg/service"
	"net/http"
	"os"

	// swagger embed files
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/subosito/gotenv"
)

// @title           Базы данных библиотеки
// @version         1.0
// @description     Данную задачу обычно предлагают в компании Ozon, которая раньше продавала книги.

// @host      localhost:8080

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing configs: %s", err.Error())
	}

	if err := gotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variables: %s", err.Error())
	}

	db, err := repository.NewPostgresDB(repository.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
		Password: os.Getenv("DB_PASSWORD"),
	})

	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())
	}

	bookRepository := repository.NewRepository(db)

	err = bookRepository.InitializeAuthors()
	if err != nil {
		log.Fatalf("Ошибка инициализации авторов: %v", err)
	}
	err = bookRepository.AddBooksIfEmpty()
	if err != nil {
		log.Fatalf("Ошибка инициализации: %v", err)
	}
	err = bookRepository.InitializeUsers()
	if err != nil {
		log.Fatalf("Ошибка инициализации пользователей: %v", err)
	}

	libraryService := service.NewLibraryService(bookRepository)
	libraryHandler := handler.NewLibraryHandler(libraryService)

	r := gin.Default()

	handler.InitRoutes(r, libraryHandler)

	log.Printf("Starting server at port 8080")
	err = http.ListenAndServe(":8080", r)
	if err != nil {
		log.Fatal(err)
	}

}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
