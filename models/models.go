package models

type Book struct {
	ID           int    `json:"id"`
	Title        string `json:"title"`
	AuthorID     int    `json:"author_id" db:"author_id"`
	RentalsCount int    `json:"rentalscount"`
	Author       Author `json:"author"`
}

type Author struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description" db:"description"`
}

type User struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	RentedBooks []Book `json:"rented_books"`
}

type AuthorRentCount struct {
	Name         string `json:"name"`
	RentalsCount int    `json:"rentalscount"`
}
